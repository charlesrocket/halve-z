+++
title = "Audio"
date = 2023-06-05
[taxonomies]
categories = ["media"]
tags = ["content", "shortcode"]
[extra]
subtitle = "Working with audio files"
+++

input:

```rs
{{/* audio(src=["over9000.ogg", "over9000.mp3"]) */}}
```

- `src`: an array of audio file paths (mandatory)

output:

```html
{{ audio(src=["over9000.ogg", "over9000.mp3"]) }}
```

{{ audio(src=["over9000.ogg", "over9000.mp3"]) }}
