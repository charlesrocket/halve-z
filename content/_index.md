+++
template = "index.html"
+++

# Info

Halve-Z is a two-column theme for Zola. It features taxonomies, automatic color schemes, project cards, and a Giscus comment system.

The [documentation](https://github.com/charlesrocket/halve-z) contains the full set of features and installation manuals.
