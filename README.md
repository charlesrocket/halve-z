# halve-z
[![Netlify Status](https://api.netlify.com/api/v1/badges/352a12ed-cdba-4545-9256-9fb698f5a94f/deploy-status?branch=trunk)](https://app.netlify.com/sites/halve-z/deploys)

A two-column theme for Zola.

## Features

* taxonomies
* auto color schemes
* ToC
* media shortcodes
* SEO
* CSP
* project cards
* comments ([giscus](http://giscus.app))
* read time

## Installation

### 1: Install

Add the theme as a git submodule:

```sh
git submodule add https://github.com/charlesrocket/halve-z themes/halve-z
```

### 2: Activate

Add `theme = "halve-z"` to your project's root config.
